################################################################################
#Linear mixed model (LMM) analysis on human protein classes, with treatment,   #
#visit and treatment x visit as fixed effects and subject as random effect.    #                                        #                                                    # 
################################################################################
#Author: Diana Hendrickx#
#########################
#input:
#- data_IBAQ_top10_rel.RData - R workspace (.RData file) with relative 
#  abundance of top 10 protein classes + other. 
#  Rows are human protein classes, columns are samples.
#- metadata.txt: metadata which contains the following columns:
#  "DAS_number" (sample ID),"Subject number","Visit","Treatment"
#  (AAF-syn vs AAF). Rows are samples.

etwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

#load R packages
library(readxl)
library(limma)
library(stringi)
library(dplyr)
library(compositions)
library(lme4)
library(lmerTest)
library(emmeans)

##As we will apply t-tests to compare visits, we have to normalize
#the data with CLR transform.
#clr transform #lowest rel abundance order 10^(-5)
clr_IBAQ_top10_rel<-matrix(clr(t(data_IBAQ_top10_rel+10^(-6))),
                           nrow=117,ncol=11)
rownames(clr_IBAQ_top10_rel)<-colnames(data_IBAQ_top10_rel)
colnames(clr_IBAQ_top10_rel)<-rownames(data_IBAQ_top10_rel)

meta<-data.frame()
for (i in 1:117){
  df<-metadata[which(metadata$DAS_number==rownames(clr_IBAQ_top10_rel[,1:10])[i]),3:6]
  meta<-rbind(meta,df)
}
rownames(meta)<-rownames(clr_IBAQ_top10_rel[,1:10])
data_LMM<-data.frame(clr_IBAQ_top10_rel[,1:10],meta)

#Immunoglobulins
mixed.lmer.Immunoglobulins <- 
  lmer(Immunoglobulins ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test1<-pairs(emmeans(mixed.lmer.Immunoglobulins,
                     "Visit", by = "Treatment"))
test2<-pairs(emmeans(mixed.lmer.Immunoglobulins,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(contrast=summary(test1)$contrast,
             Treatment=summary(test1)$Treatment,
             p1=summary(test1)$p.value)
pvals_compare_intervention_groups<-
  data.frame(contrast=summary(test2)$contrast,
             Visit=summary(test2)$Visit,
             p1=summary(test2)$p.value)

#Glycoside hydrolases
mixed.lmer.Glycoside.hydrolases <- 
  lmer(Glycoside.hydrolases ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test3<-pairs(emmeans(mixed.lmer.Glycoside.hydrolases,
                     "Visit", by = "Treatment"))
test4<-pairs(emmeans(mixed.lmer.Glycoside.hydrolases,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p2=summary(test3)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p2=summary(test4)$p.value)

#Transthyretin/hydroxyisourate hydrolases
mixed.lmer.Transthyretin.hydroxyisourate.hydrolases  <- 
  lmer(Transthyretin.hydroxyisourate.hydrolases  ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test5<-pairs(emmeans(mixed.lmer.Transthyretin.hydroxyisourate.hydrolases,
                     "Visit", by = "Treatment"))
test6<-pairs(emmeans(mixed.lmer.Transthyretin.hydroxyisourate.hydrolases,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p3=summary(test5)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p3=summary(test6)$p.value)

#Proline-rich proteins
mixed.lmer.Proline.rich.proteins   <- 
  lmer(Proline.rich.proteins   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test7<-pairs(emmeans(mixed.lmer.Proline.rich.proteins ,
                     "Visit", by = "Treatment"))
test8<-pairs(emmeans(mixed.lmer.Proline.rich.proteins ,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p4=summary(test7)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p4=summary(test8)$p.value)

#S100 proteins
mixed.lmer.S100.proteins   <- 
  lmer(S100.proteins   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test9<-pairs(emmeans(mixed.lmer.S100.proteins ,
                     "Visit", by = "Treatment"))
test10<-pairs(emmeans(mixed.lmer.S100.proteins ,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p5=summary(test9)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p5=summary(test10)$p.value)

#Secretory proteins
mixed.lmer.Secretory.proteins   <- 
  lmer(Secretory.proteins   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test11<-pairs(emmeans(mixed.lmer.Secretory.proteins ,
                     "Visit", by = "Treatment"))
test12<-pairs(emmeans(mixed.lmer.Secretory.proteins ,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p6=summary(test11)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p6=summary(test12)$p.value)

#Serine peptidases
mixed.lmer.Serine.peptidases   <- 
  lmer(Serine.peptidases   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test13<-pairs(emmeans(mixed.lmer.Serine.peptidases ,
                      "Visit", by = "Treatment"))
test14<-pairs(emmeans(mixed.lmer.Serine.peptidases ,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p7=summary(test13)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p7=summary(test14)$p.value)

#Carboxypeptidases
mixed.lmer.Carboxypeptidases   <- 
  lmer(Carboxypeptidases   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test15<-pairs(emmeans(mixed.lmer.Carboxypeptidases ,
                      "Visit", by = "Treatment"))
test16<-pairs(emmeans(mixed.lmer.Carboxypeptidases ,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p8=summary(test15)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p8=summary(test16)$p.value)

#Dipeptidyl peptidases
mixed.lmer.Dipeptidyl.peptidases   <- 
  lmer(Dipeptidyl.peptidases   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test17<-pairs(emmeans(mixed.lmer.Dipeptidyl.peptidases ,
                      "Visit", by = "Treatment"))
test18<-pairs(emmeans(mixed.lmer.Dipeptidyl.peptidases ,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p9=summary(test17)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p9=summary(test18)$p.value)

#Actin family
mixed.lmer.Actin.family   <- 
  lmer(Actin.family   ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test19<-pairs(emmeans(mixed.lmer.Actin.family ,
                      "Visit", by = "Treatment"))
test20<-pairs(emmeans(mixed.lmer.Actin.family ,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p10=summary(test19)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p10=summary(test20)$p.value)

#adjusted p-vals
#T0M - T12M (AAF-syn)
pvals1<-pvals_compare_visits[1,3:12]
adjpvals1<-p.adjust(pvals1,method = "BH")

#T0M - T6M  (AAF-syn)
pvals2<-pvals_compare_visits[2,3:12]
adjpvals2<-p.adjust(pvals2,method = "BH")

#T12M - T6M (AAF-syn)
pvals3<-pvals_compare_visits[3,3:12]
adjpvals3<-p.adjust(pvals3,method = "BH")

#T0M - T12M (AAF)
pvals4<-pvals_compare_visits[4,3:12]
adjpvals4<-p.adjust(pvals4,method = "BH")

#T0M - T6M (AAF)
pvals5<-pvals_compare_visits[5,3:12]
adjpvals5<-p.adjust(pvals5,method = "BH")

#T12M - T6M (AAF)
pvals6<-pvals_compare_visits[6,3:12]
adjpvals6<-p.adjust(pvals6,method = "BH")

#A - B (baseline)
pvals7<-pvals_compare_intervention_groups[1,3:12]
adjpvals7<-p.adjust(pvals7,method = "BH")

#A - B (12 months)
pvals8<-pvals_compare_intervention_groups[2,3:12]
adjpvals8<-p.adjust(pvals8,method = "BH")

#A - B (6 months)
pvals9<-pvals_compare_intervention_groups[3,3:12]
adjpvals9<-p.adjust(pvals9,method = "BH")

