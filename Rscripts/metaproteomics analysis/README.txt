partial_RDA_taxonomy.R:
- Partial RDA at the taxonomic family level with intervention as explanatory variable,adjusting for other environmental factors. 
- Figure S5G: partial RDA at visit 6 months  
- Figure S5H: partial RDA at visit 12 months 

partial_RDA_proteome.R:
- Partial RDA at the proteome level with intervention as explanatory variable,adjusting for other environmental factors.
- Figure S5A: partial RDA at visit 6 months 
- Figure S5B: partial RDA at visit 12 months 

partial_RDA_functional.R:
- Partial RDA at the protein functional level with intervention as explanatory variable, adjusting for other environmental factors. 
- Figure S5C: partial RDA at visit 6 months 
- Figure S5D: partial RDA at visit 6 months 

prepare_for_LEfSe.R:
- Prepare input files for the LEfSe Galaxy module

LMM_taxonomy_level.R:
- Linear mixed model (LMM) analysis on core taxa
- treatment, visit and treatment x visit as fixed effects
- subject as random effect.

LMM_functional_level.R:
- Linear mixed model (LMM) analysis on top 10 microbial functional protein classes
- treatment, visit and treatment x visit as fixed effects
- subject as random effect. 

LMM_all_functional_classes.R:
- Linear mixed model (LMM) analysis on all microbial functional protein classes
- treatment, visit and treatment x visit as fixed effects
- subject as random effect. 

LMM_human_protein_classes.R:
- Linear mixed model (LMM) analysis on human protein classes
- treatment, visit and treatment x visit as fixed effects
- subject as random effect. 

LMM_bifido_CAZymes.R:
- Linear mixed model (LMM) analysis on bifidobacterial CAZymes
- treatment, visit and treatment x visit as fixed effects
- subject as random effect.

LMM_bifido_AAmetabolism.R
- Linear mixed model (LMM) analysis on bifidobacterial amino acid metabolism proteins
- treatment,visit and treatment x visit as fixed effects
- subject as random effect. 

LMM_non_bifido_CAZymes.R
- Linear mixed model (LMM) analysis on non-bifidobacterial CAZymes
- treatment, visit and treatment x visit as fixed effects
- subject as random effect

LMM_non_bifido_AAmetabolism.R
- Linear mixed model (LMM) analysis on non-bifidobacterial amino acid metabolism proteins
- treatment, visit and treatment x visit as fixed effects
- subject as random effect

heatmap_6M.R: 
- Make a heatmap for the LEfSe output at visit 6 months (Figure S3A).

heatmap_12M.R: 
- Make a heatmap for the LEfSe output at visit 12 months (Figure S3B).