################################################################################
#Linear mixed model (LMM) analysis on non-bifidobacterial amino acid metabolism#
#proteins with treatment,visit and treatment x visit as fixed effects and      #
#subject as random effect.                                                     # 
################################################################################
#Author: Diana Hendrickx#
#########################
#input:
#- data_IBAQ_microbial_rel.RData - R workspace (.RData file) with relative 
#  abundances at the microbial proteome level.
#  Rows are microbial proteins, columns are samples.
#- metadata.txt: metadata which contains the following columns:
#  "DAS_number" (sample ID),"Subject number","Visit","Treatment"
#  (AAF-syn vs AAF). Rows are samples.
#- amino_acid_metabolism.xlsx: list of non-bifidobacterial amino acid metabolism
#  proteins observed by metaproteomics

setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

#load R packages
library(readxl)
library(limma)
library(stringi)
library(dplyr)
library(compositions)
library(lme4)
library(lmerTest)
library(emmeans)

#clr transform data_IBAQ_microbial_rel
#smallest relative abundance order 1e-07 => ad 1e-08
library(compositions)
clr_IBAQ_microbial_rel<-matrix(clr(data_IBAQ_microbial_rel 
                                   + 10^(-8)),nrow=117,ncol=1653)
rownames(clr_IBAQ_microbial_rel)<-rownames(data_IBAQ_microbial_rel)
colnames(clr_IBAQ_microbial_rel)<-colnames(data_IBAQ_microbial_rel)

#select non-bifidobacterial proteins
ind_bifido<-which(stri_detect_regex(colnames(clr_IBAQ_microbial_rel),
                                    "Bifidobacterium")==TRUE)
clr_IBAQ_microbial_rel_non_bifi<-clr_IBAQ_microbial_rel[,-ind_bifido]

#make matrix of amino acid metabolism proteins
clr_AA_mat<-matrix(data=NA,nrow=117,ncol=21)

for (i in 1:21){
  ind_AA<-which(stri_detect_regex(colnames(clr_IBAQ_microbial_rel_non_bifi),AA$...1[i])==TRUE)
  if (length(ind_AA) > 1){
    clr_AA_mat[,i]<-rowMeans(clr_IBAQ_microbial_rel_non_bifi[,ind_AA])
  } else {
    clr_AA_mat[,i]<-clr_IBAQ_microbial_rel_non_bifi[,ind_AA]
  }
}
rownames(clr_AA_mat)<-rownames(clr_IBAQ_microbial_rel_non_bifi)
colnames(clr_AA_mat)<-as.character(AA$...1)

meta<-data.frame()
for (i in 1:117){
  df<-metadata[which(metadata$DAS_number==rownames(clr_AA_mat)[i]),3:6]
  meta<-rbind(meta,df)
}
rownames(meta)<-rownames(clr_AA_mat)
data_LMM<-data.frame(clr_AA_mat,meta)

#2,3-bisphosphoglycerate-independent phosphoglycerate mutase
mixed.lmer.X2.3.bisphosphoglycerate.independent.phosphoglycerate.mutase <- 
  lmer(X2.3.bisphosphoglycerate.independent.phosphoglycerate.mutase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test1<-pairs(emmeans(mixed.lmer.X2.3.bisphosphoglycerate.independent.phosphoglycerate.mutase,
                     "Visit", by = "Treatment"))
test2<-pairs(emmeans(mixed.lmer.X2.3.bisphosphoglycerate.independent.phosphoglycerate.mutase,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(contrast=summary(test1)$contrast,
             Treatment=summary(test1)$Treatment,
             p1=summary(test1)$p.value)
pvals_compare_intervention_groups<-
  data.frame(contrast=summary(test2)$contrast,
             Visit=summary(test2)$Visit,
             p1=summary(test2)$p.value)

#3-hydroxyacyl-CoA dehydrogenase
mixed.lmer.X3.hydroxyacyl.CoA.dehydrogenase <- 
  lmer(X3.hydroxyacyl.CoA.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test3<-pairs(emmeans(mixed.lmer.X3.hydroxyacyl.CoA.dehydrogenase,
                     "Visit", by = "Treatment"))
test4<-pairs(emmeans(mixed.lmer.X3.hydroxyacyl.CoA.dehydrogenase,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p2=summary(test3)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p2=summary(test4)$p.value)

#3-hydroxybutyryl-CoA dehydrogenase
mixed.lmer.X3.hydroxybutyryl.CoA.dehydrogenase <- 
  lmer(X3.hydroxybutyryl.CoA.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test5<-pairs(emmeans(mixed.lmer.X3.hydroxybutyryl.CoA.dehydrogenase,
                     "Visit", by = "Treatment"))
test6<-pairs(emmeans(mixed.lmer.X3.hydroxybutyryl.CoA.dehydrogenase,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p3=summary(test5)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p3=summary(test6)$p.value)

#3-isopropylmalate dehydrogenase
mixed.lmer.X3.isopropylmalate.dehydrogenase <- 
  lmer(X3.isopropylmalate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test7<-pairs(emmeans(mixed.lmer.X3.isopropylmalate.dehydrogenase,
                     "Visit", by = "Treatment"))
test8<-pairs(emmeans(mixed.lmer.X3.isopropylmalate.dehydrogenase,
                     "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p4=summary(test7)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,p4=summary(test8)$p.value)

#5-methyltetrahydropteroyltriglutamate--homocysteine methyltransferase
mixed.lmer.X5.methyltetrahydropteroyltriglutamate..homocysteine.methyltransferase <- 
  lmer(X5.methyltetrahydropteroyltriglutamate..homocysteine.methyltransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test9<-pairs(emmeans(mixed.lmer.X5.methyltetrahydropteroyltriglutamate..homocysteine.methyltransferase,
                     "Visit", by = "Treatment"))
test10<-pairs(emmeans(mixed.lmer.X5.methyltetrahydropteroyltriglutamate..homocysteine.methyltransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p5=summary(test9)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p5=summary(test10)$p.value)

#6-phosphogluconate dehydrogenase, decarboxylating
mixed.lmer.X6.phosphogluconate.dehydrogenase..decarboxylating <- 
  lmer(X6.phosphogluconate.dehydrogenase..decarboxylating ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test11<-pairs(emmeans(mixed.lmer.X6.phosphogluconate.dehydrogenase..decarboxylating,
                      "Visit", by = "Treatment"))
test12<-pairs(emmeans(mixed.lmer.X6.phosphogluconate.dehydrogenase..decarboxylating,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p6=summary(test11)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p6=summary(test12)$p.value)

#Acetate kinase
mixed.lmer.Acetate.kinase <- 
  lmer(Acetate.kinase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test13<-pairs(emmeans(mixed.lmer.Acetate.kinase,
                      "Visit", by = "Treatment"))
test14<-pairs(emmeans(mixed.lmer.Acetate.kinase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p7=summary(test13)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p7=summary(test14)$p.value)

#Acetyl-CoA acetyltransferase
mixed.lmer.Acetyl.CoA.acetyltransferase <- 
  lmer(Acetyl.CoA.acetyltransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test15<-pairs(emmeans(mixed.lmer.Acetyl.CoA.acetyltransferase,
                      "Visit", by = "Treatment"))
test16<-pairs(emmeans(mixed.lmer.Acetyl.CoA.acetyltransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p8=summary(test15)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p8=summary(test16)$p.value)

#Acyl-CoA dehydrogenase
mixed.lmer.Acyl.CoA.dehydrogenase <- 
  lmer(Acyl.CoA.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test17<-pairs(emmeans(mixed.lmer.Acyl.CoA.dehydrogenase,
                      "Visit", by = "Treatment"))
test18<-pairs(emmeans(mixed.lmer.Acyl.CoA.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p9=summary(test17)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p9=summary(test18)$p.value)

#adenylosuccinate lyase
mixed.lmer.adenylosuccinate.lyase <- 
  lmer(adenylosuccinate.lyase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test19<-pairs(emmeans(mixed.lmer.adenylosuccinate.lyase,
                      "Visit", by = "Treatment"))
test20<-pairs(emmeans(mixed.lmer.adenylosuccinate.lyase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p10=summary(test19)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p10=summary(test20)$p.value)

#adenylosuccinate synthase
mixed.lmer.adenylosuccinate.synthase <- 
  lmer(adenylosuccinate.synthase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test21<-pairs(emmeans(mixed.lmer.adenylosuccinate.synthase,
                      "Visit", by = "Treatment"))
test22<-pairs(emmeans(mixed.lmer.adenylosuccinate.synthase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p11=summary(test21)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p11=summary(test22)$p.value)

#Aldehyde-alcohol dehydrogenase
mixed.lmer.Aldehyde.alcohol.dehydrogenase <- 
  lmer(Aldehyde.alcohol.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test23<-pairs(emmeans(mixed.lmer.Aldehyde.alcohol.dehydrogenase,
                      "Visit", by = "Treatment"))
test24<-pairs(emmeans(mixed.lmer.Aldehyde.alcohol.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p12=summary(test23)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p12=summary(test24)$p.value)

#Argininosuccinate lyase
mixed.lmer.Argininosuccinate.lyase <- 
  lmer(Argininosuccinate.lyase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test25<-pairs(emmeans(mixed.lmer.Argininosuccinate.lyase,
                      "Visit", by = "Treatment"))
test26<-pairs(emmeans(mixed.lmer.Argininosuccinate.lyase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p13=summary(test25)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p13=summary(test26)$p.value)

#Argininosuccinate synthase
mixed.lmer.Argininosuccinate.synthase <- 
  lmer(Argininosuccinate.synthase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test27<-pairs(emmeans(mixed.lmer.Argininosuccinate.synthase,
                      "Visit", by = "Treatment"))
test28<-pairs(emmeans(mixed.lmer.Argininosuccinate.synthase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p14=summary(test27)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p14=summary(test28)$p.value)

#Aspartate carbamoyltransferase
mixed.lmer.Aspartate.carbamoyltransferase <- 
  lmer(Aspartate.carbamoyltransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test29<-pairs(emmeans(mixed.lmer.Aspartate.carbamoyltransferase,
                      "Visit", by = "Treatment"))
test30<-pairs(emmeans(mixed.lmer.Aspartate.carbamoyltransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p15=summary(test29)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p15=summary(test30)$p.value)

#Aspartate-semialdehyde dehydrogenase
mixed.lmer.Aspartate.semialdehyde.dehydrogenase <- 
  lmer(Aspartate.semialdehyde.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test31<-pairs(emmeans(mixed.lmer.Aspartate.semialdehyde.dehydrogenase,
                      "Visit", by = "Treatment"))
test32<-pairs(emmeans(mixed.lmer.Aspartate.semialdehyde.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p16=summary(test31)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p16=summary(test32)$p.value)

#Aspartokinase
mixed.lmer.Aspartokinase <- 
  lmer(Aspartokinase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test33<-pairs(emmeans(mixed.lmer.Aspartokinase,
                      "Visit", by = "Treatment"))
test34<-pairs(emmeans(mixed.lmer.Aspartokinase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p17=summary(test33)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p17=summary(test34)$p.value)

#bifunctional acetaldehyde-CoA/alcohol dehydrogenase
mixed.lmer.bifunctional.acetaldehyde.CoA.alcohol.dehydrogenase <- 
  lmer(bifunctional.acetaldehyde.CoA.alcohol.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test35<-pairs(emmeans(mixed.lmer.bifunctional.acetaldehyde.CoA.alcohol.dehydrogenase,
                      "Visit", by = "Treatment"))
test36<-pairs(emmeans(mixed.lmer.bifunctional.acetaldehyde.CoA.alcohol.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p18=summary(test35)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p18=summary(test36)$p.value)

#branched-chain amino acid aminotransferase
mixed.lmer.branched.chain.amino.acid.aminotransferase <- 
  lmer(branched.chain.amino.acid.aminotransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test37<-pairs(emmeans(mixed.lmer.branched.chain.amino.acid.aminotransferase,
                      "Visit", by = "Treatment"))
test38<-pairs(emmeans(mixed.lmer.branched.chain.amino.acid.aminotransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p19=summary(test37)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p19=summary(test38)$p.value)

#Catalase
mixed.lmer.Catalase <- 
  lmer(Catalase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test39<-pairs(emmeans(mixed.lmer.Catalase,
                      "Visit", by = "Treatment"))
test40<-pairs(emmeans(mixed.lmer.Catalase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p20=summary(test39)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p20=summary(test40)$p.value)

#Cysteine synthase
mixed.lmer.Cysteine.synthase <- 
  lmer(Cysteine.synthase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test41<-pairs(emmeans(mixed.lmer.Cysteine.synthase,
                      "Visit", by = "Treatment"))
test42<-pairs(emmeans(mixed.lmer.Cysteine.synthase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p21=summary(test41)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p21=summary(test42)$p.value)

#D-3-phosphoglycerate dehydrogenase
mixed.lmer.D.3.phosphoglycerate.dehydrogenase <- 
  lmer(D.3.phosphoglycerate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test43<-pairs(emmeans(mixed.lmer.D.3.phosphoglycerate.dehydrogenase,
                      "Visit", by = "Treatment"))
test44<-pairs(emmeans(mixed.lmer.D.3.phosphoglycerate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p22=summary(test43)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p22=summary(test44)$p.value)

#Dihydropyrimidine dehydrogenase
mixed.lmer.Dihydropyrimidine.dehydrogenase <- 
  lmer(Dihydropyrimidine.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test45<-pairs(emmeans(mixed.lmer.Dihydropyrimidine.dehydrogenase,
                      "Visit", by = "Treatment"))
test46<-pairs(emmeans(mixed.lmer.Dihydropyrimidine.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p23=summary(test45)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p23=summary(test46)$p.value)

#Glutamate dehydrogenase
mixed.lmer.Glutamate.dehydrogenase <- 
  lmer(Glutamate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test47<-pairs(emmeans(mixed.lmer.Glutamate.dehydrogenase,
                      "Visit", by = "Treatment"))
test48<-pairs(emmeans(mixed.lmer.Glutamate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p24=summary(test47)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p24=summary(test48)$p.value)

#Glutamate synthase [NADPH] small chain
mixed.lmer.Glutamate.synthase...NADPH...small.chain <- 
  lmer(Glutamate.synthase...NADPH...small.chain ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test49<-pairs(emmeans(mixed.lmer.Glutamate.synthase...NADPH...small.chain,
                      "Visit", by = "Treatment"))
test50<-pairs(emmeans(mixed.lmer.Glutamate.synthase...NADPH...small.chain,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p25=summary(test49)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p25=summary(test50)$p.value)

#Glutamate synthase domain 2
mixed.lmer.Glutamate.synthase.domain.2 <- 
  lmer(Glutamate.synthase.domain.2 ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test51<-pairs(emmeans(mixed.lmer.Glutamate.synthase.domain.2,
                      "Visit", by = "Treatment"))
test52<-pairs(emmeans(mixed.lmer.Glutamate.synthase.domain.2,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p26=summary(test51)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p26=summary(test52)$p.value)

#Glutamate--ammonia ligase, catalytic domain protein
mixed.lmer.Glutamate..ammonia.ligase..catalytic.domain.protein <- 
  lmer(Glutamate..ammonia.ligase..catalytic.domain.protein ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test53<-pairs(emmeans(mixed.lmer.Glutamate..ammonia.ligase..catalytic.domain.protein,
                      "Visit", by = "Treatment"))
test54<-pairs(emmeans(mixed.lmer.Glutamate..ammonia.ligase..catalytic.domain.protein,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p27=summary(test53)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p27=summary(test54)$p.value)

#Glutamine synthetase
mixed.lmer.Glutamine.synthetase <- 
  lmer(Glutamine.synthetase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test55<-pairs(emmeans(mixed.lmer.Glutamine.synthetase,
                      "Visit", by = "Treatment"))
test56<-pairs(emmeans(mixed.lmer.Glutamine.synthetase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p28=summary(test55)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p28=summary(test56)$p.value)

#Glycine cleavage system H protein
mixed.lmer.Glycine.cleavage.system.H.protein <- 
  lmer(Glycine.cleavage.system.H.protein ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test57<-pairs(emmeans(mixed.lmer.Glycine.cleavage.system.H.protein,
                      "Visit", by = "Treatment"))
test58<-pairs(emmeans(mixed.lmer.Glycine.cleavage.system.H.protein,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p29=summary(test57)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p29=summary(test58)$p.value)

#ketol-acid reductoisomerase
mixed.lmer.ketol.acid.reductoisomerase <- 
  lmer(ketol.acid.reductoisomerase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test59<-pairs(emmeans(mixed.lmer.ketol.acid.reductoisomerase,
                      "Visit", by = "Treatment"))
test60<-pairs(emmeans(mixed.lmer.ketol.acid.reductoisomerase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p30=summary(test59)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p30=summary(test60)$p.value)

#L-asparaginase
mixed.lmer.L.asparaginase <- 
  lmer(L.asparaginase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test61<-pairs(emmeans(mixed.lmer.L.asparaginase,
                      "Visit", by = "Treatment"))
test62<-pairs(emmeans(mixed.lmer.L.asparaginase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p31=summary(test61)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p31=summary(test62)$p.value)

#L-lactate dehydrogenase
mixed.lmer.L.lactate.dehydrogenase <- 
  lmer(L.lactate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test63<-pairs(emmeans(mixed.lmer.L.lactate.dehydrogenase,
                      "Visit", by = "Treatment"))
test64<-pairs(emmeans(mixed.lmer.L.lactate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p32=summary(test63)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p32=summary(test64)$p.value)

#LL-diaminopimelate aminotransferase
mixed.lmer.LL.diaminopimelate.aminotransferase <- 
  lmer(LL.diaminopimelate.aminotransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test65<-pairs(emmeans(mixed.lmer.LL.diaminopimelate.aminotransferase,
                      "Visit", by = "Treatment"))
test66<-pairs(emmeans(mixed.lmer.LL.diaminopimelate.aminotransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p33=summary(test65)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p33=summary(test66)$p.value)

#malate dehydrogenase
mixed.lmer.malate.dehydrogenase <- 
  lmer(malate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test67<-pairs(emmeans(mixed.lmer.malate.dehydrogenase,
                      "Visit", by = "Treatment"))
test68<-pairs(emmeans(mixed.lmer.malate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p34=summary(test67)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p34=summary(test68)$p.value)

#methionine adenosyltransferase
mixed.lmer.methionine.adenosyltransferase <- 
  lmer(methionine.adenosyltransferase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test69<-pairs(emmeans(mixed.lmer.methionine.adenosyltransferase,
                      "Visit", by = "Treatment"))
test70<-pairs(emmeans(mixed.lmer.methionine.adenosyltransferase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p35=summary(test69)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p35=summary(test70)$p.value)

#Methionine synthase II (Cobalamin-independent)
mixed.lmer.Methionine.synthase.II...Cobalamin.independent.. <- 
  lmer(Methionine.synthase.II...Cobalamin.independent.. ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test71<-pairs(emmeans(mixed.lmer.Methionine.synthase.II...Cobalamin.independent..,
                      "Visit", by = "Treatment"))
test72<-pairs(emmeans(mixed.lmer.Methionine.synthase.II...Cobalamin.independent..,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p36=summary(test71)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p36=summary(test72)$p.value)

#methionine--tRNA ligase
mixed.lmer.methionine..tRNA.ligase <- 
  lmer(methionine..tRNA.ligase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test73<-pairs(emmeans(mixed.lmer.methionine..tRNA.ligase,
                      "Visit", by = "Treatment"))
test74<-pairs(emmeans(mixed.lmer.methionine..tRNA.ligase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p37=summary(test73)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p37=summary(test74)$p.value)

#NADP-dependent isocitrate dehydrogenase
mixed.lmer.NADP.dependent.isocitrate.dehydrogenase <- 
  lmer(NADP.dependent.isocitrate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test75<-pairs(emmeans(mixed.lmer.NADP.dependent.isocitrate.dehydrogenase,
                      "Visit", by = "Treatment"))
test76<-pairs(emmeans(mixed.lmer.NADP.dependent.isocitrate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p38=summary(test75)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p38=summary(test76)$p.value)

#NADP-specific glutamate dehydrogenase
mixed.lmer.NADP.specific.glutamate.dehydrogenase <- 
  lmer(NADP.specific.glutamate.dehydrogenase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test77<-pairs(emmeans(mixed.lmer.NADP.specific.glutamate.dehydrogenase,
                      "Visit", by = "Treatment"))
test78<-pairs(emmeans(mixed.lmer.NADP.specific.glutamate.dehydrogenase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p39=summary(test77)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p39=summary(test78)$p.value)

#O-acetylhomoserine aminocarboxypropyltransferase/cysteine synthase
mixed.lmer.O.acetylhomoserine.aminocarboxypropyltransferase.cysteine.synthase <- 
  lmer(O.acetylhomoserine.aminocarboxypropyltransferase.cysteine.synthase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test79<-pairs(emmeans(mixed.lmer.O.acetylhomoserine.aminocarboxypropyltransferase.cysteine.synthase,
                      "Visit", by = "Treatment"))
test80<-pairs(emmeans(mixed.lmer.O.acetylhomoserine.aminocarboxypropyltransferase.cysteine.synthase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p40=summary(test79)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p40=summary(test80)$p.value)

#Sulfide dehydrogenase (Flavoprotein) subunit SudA
mixed.lmer.Sulfide.dehydrogenase...Flavoprotein...subunit.SudA <- 
  lmer(Sulfide.dehydrogenase...Flavoprotein...subunit.SudA ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test81<-pairs(emmeans(mixed.lmer.Sulfide.dehydrogenase...Flavoprotein...subunit.SudA,
                      "Visit", by = "Treatment"))
test82<-pairs(emmeans(mixed.lmer.Sulfide.dehydrogenase...Flavoprotein...subunit.SudA,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p41=summary(test81)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p41=summary(test82)$p.value)

#Threonine synthase
mixed.lmer.Threonine.synthase <- 
  lmer(Threonine.synthase ~ Treatment +
         Visit + Treatment*Visit + (1|Subject.number), 
       data = data_LMM)
test83<-pairs(emmeans(mixed.lmer.Threonine.synthase,
                      "Visit", by = "Treatment"))
test84<-pairs(emmeans(mixed.lmer.Threonine.synthase,
                      "Treatment", by = "Visit"))
pvals_compare_visits<-
  data.frame(pvals_compare_visits,p42=summary(test83)$p.value)
pvals_compare_intervention_groups<-
  data.frame(pvals_compare_intervention_groups,
             p42=summary(test84)$p.value)

#adjusted p-vals
#T0M - T12M (AAF-syn)
pvals1<-pvals_compare_visits[1,3:44]
adjpvals1<-p.adjust(pvals1,method = "BH")

#T0M - T6M  (AAF-syn)
pvals2<-pvals_compare_visits[2,3:44]
adjpvals2<-p.adjust(pvals2,method = "BH")

#T12M - T6M (AAF-syn)
pvals3<-pvals_compare_visits[3,3:44]
adjpvals3<-p.adjust(pvals3,method = "BH")

#T0M - T12M (AAF)
pvals4<-pvals_compare_visits[4,3:44]
adjpvals4<-p.adjust(pvals4,method = "BH")

#T0M - T6M (AAF)
pvals5<-pvals_compare_visits[5,3:44]
adjpvals5<-p.adjust(pvals5,method = "BH")

#T12M - T6M (AAF)
pvals6<-pvals_compare_visits[6,3:44]
adjpvals6<-p.adjust(pvals6,method = "BH")

#A - B (baseline)
pvals7<-pvals_compare_intervention_groups[1,3:44]
adjpvals7<-p.adjust(pvals7,method = "BH")

#A - B (12 months)
pvals8<-pvals_compare_intervention_groups[2,3:44]
adjpvals8<-p.adjust(pvals8,method = "BH")

#A - B (6 months)
pvals9<-pvals_compare_intervention_groups[3,3:44]
adjpvals9<-p.adjust(pvals9,method = "BH")