################################################################################
#Partial RDA at the protein functional level with intervention as explanatory  #
#variable, adjusting for other environmental factors.                          #                                 #                            #
#                                                                              #
#Figure S5C: partial RDA at visit 6 months                                     #
#Figure S5D: partial RDA at visit 12 months                                    #
################################################################################
#Author: Diana Hendrickx#
#########################
#input:
#An .RData file including:
# - clindat_0M: a data frame with the clinical data for baseline visit.
#   row names are subject IDs, column names are names of the clinical
#   variables.
#   order of the variables used in this script: "sex","age",
#   "delivery","alrgymot","alrgyfat","sibl","SCORADMAN","FOODTRIG4",
#   "FOODTRIG2","FOODTRIG5","FOODTRIG7","FOODTRIG8","FOODTRIG3",
#   "SPTOCM","SPTOSB","SPTOWF","SPTOP" 
# - clindat_6M: a data frame with the clinical data for visit 6 months.
#   row names are subject IDs, column names are names of the clinical
#   variables.
#   order of the variables used in this script:"treatment","sex","age",
#   "delivery","alrgymot","alrgyfat","sibl","FOODTRIG4","FOODTRIG2",    
#   "FOODTRIG5","FOODTRIG7","FOODTRIG8","FOODTRIG3","SPTOCM","SPTOSB",    
#   "SPTOWF","SPTOP","VOMITING","SPITTING","STOOLFREQ","STOOLCONSIST",
#   "STOOLCOLOUR","GASWIND","SCORADMAN","num_ab","num_inf"
# - clindat_12M: a data frame with the clinical data for visit 12 months.
#   row names are subject IDs, column names are names of the clinical
#   variables.
#   order of the variables used in this script: "treatment","sex","age",          
#   "delivery","alrgymot","alrgyfat","sibl","FOODTRIG4","FOODTRIG2",
#   "FOODTRIG5","FOODTRIG7","FOODTRIG8","FOODTRIG3","VOMITING","SPITTING",     
#   "STOOLFREQ","STOOLCONSIST","STOOLCOLOUR","GASWIND","SCORADMAN",
#   "TOLCMVIS","SPTOCM","SPTOSB","SPTOWF","SPTOP","num_ab","num_inf"
# - data_IBAQ_categories_rel: relative abundances at the functional level (KEGG
#   BRITE level c). Rows are samples, columns are protein functional classes.
# - metadata: metadata which contains the following columns:
#   "DAS_number" (sample ID),"Subject number","Visit","TOLCMVIS 12M"
#   (tolerance to cow's milk Yes/No). Rows are samples.

#set work directory to directory with input files
setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

library(limma)
library(dplyr)
library(factoextra)
library(vegan)
library(stringi)
library(compositions)

#data per time point
baseline<-which(metadata$Visit=="T0M" & metadata$TOLCMVIS.12M!="")
six_months<-which(metadata$Visit=="T6M" & metadata$TOLCMVIS.12M!="")
twelve_months<-which(metadata$Visit=="T12M" & metadata$TOLCMVIS.12M!="")

sampleID_0M<-as.character(metadata$DAS_number)[baseline]
sampleID_6M<-as.character(metadata$DAS_number)[six_months]
sampleID_12M<-as.character(metadata$DAS_number)[twelve_months]

ind_0M<-which(rownames(data_IBAQ_categories_rel) %in% sampleID_0M)
ind_6M<-which(rownames(data_IBAQ_categories_rel) %in% sampleID_6M)
ind_12M<-which(rownames(data_IBAQ_categories_rel) %in% sampleID_12M)

#clr transform 
clr_IBAQ_categories_rel<-matrix(clr(data_IBAQ_categories_rel+10^(-7)),nrow=120,
                                ncol=dim(data_IBAQ_categories_rel)[2])
rownames(clr_IBAQ_categories_rel)<-rownames(data_IBAQ_categories_rel)
colnames(clr_IBAQ_categories_rel)<-colnames(data_IBAQ_categories_rel)

########################################
#Perform partial RDA at visit 6 months.#
########################################
set.seed(123)
partial_rda_treatment_6M<-rda(rel_abundance_6M ~ treatment+
                                Condition(outgrowth_CM+sex+age+delivery+
                                            alrgymot+alrgyfat+
                                            sibl+egg_allergy+other_allergy+
                                            VOMITING+
                                            SPITTING+STOOLFREQ+STOOLCONSIST+
                                            STOOLCOLOUR+
                                          GASWIND+SCORADMAN+num_ab+num_inf),
                              data = meta_6M, na.action = na.omit)
partial_rda_treatment_6M$anova$`Pr(>F)` <- p.adjust(
  partial_rda_treatment_6M$anova$`Pr(>F)`, method = "fdr") 
# adjust p-values for multiple testing
print(anova(partial_rda_treatment_6M,by="term"))

perc <- round(100*(summary(partial_rda_treatment_6M)$cont$importance[2, 1:2]), 2)
print(perc[1])

#Figure S5C
loadings<-rbind(scores(partial_rda_treatment_6M, display = "bp", scaling = 0),
                scores(partial_rda_treatment_6M, display = "species", scaling = 0))
loading_contributions<-rowSums(abs(loadings))
rel_loading_contributions<-loading_contributions/sum(loading_contributions)
rel_loading_contributions[1]

ordered_rel_loading_contr<-rel_loading_contributions[order(rel_loading_contributions,
                                                           decreasing=TRUE)]
cum_loading_contrib<-vector(mode="numeric",
                            length=length(ordered_rel_loading_contr))
for (i in 1:length(ordered_rel_loading_contr)){
  cum_loading_contrib<-cumsum(ordered_rel_loading_contr[1:i])
}
ind_80<-min(which(cum_loading_contrib>0.80))

sc_bp<-scores(partial_rda_treatment_6M, display = "bp", scaling = 0)
sc_sp<-scores(partial_rda_treatment_6M, display = "species", scaling = 0)
sc_si<-scores(partial_rda_treatment_6M, display = "sites", scaling = 0)

plot(partial_rda_treatment_6M,
     scaling = 2, # set scaling type 
     type = "none", # this excludes the plotting of any points from the results
     frame = FALSE,
     # set axis limits
     xlim = c(-0.3,0.3), 
     ylim = c(-0.7,0.7),
     # label the plot (title, and axes)
     main = "proteome - 6 months",
     xlab = paste0("RDA1 (", perc[1], "%)"), 
     ylab = paste0("PC1 (", perc[2], "%)") 
)
# add points for species scores
points(sc_sp, 
       pch = 22, # set shape (here, square with a fill colour)
       col = "orange",
       cex = 1.2)

# add points for site scores
points(sc_si, 
       pch = 16, # set shape (here, circle with a fill colour)
       col = as.factor(meta_6M$treatment),
       cex = 1.2) # size
# add arrows for effects of the expanatory variables
arrows(0,0, # start them from (0,0)
       sc_bp[,1], sc_bp[,2], # end them at the score value
       col = "blue", 
       lwd = 1)
# add text labels for arrows
text(x = sc_bp[,1] -0.1, # adjust text coordinate to avoid overlap with arrow tip
     y = sc_bp[,2] - 0.03, 
     labels = rownames(sc_bp), 
     col = "blue", 
     cex = 1, 
     font = 1)
legend(-1.6,0.4,legend=c("A", "B"),
       col=c("black","red"),
       pch = 16,
       cex=0.8)

#########################################
#Perform partial RDA at visit 12 months.#
#########################################
set.seed(123)
partial_rda_treatment_12M<-rda(rel_abundance_12M ~ treatment+
                                 Condition(outgrowth_CM+sex+age+delivery+
                                             alrgymot+alrgyfat+
                                             sibl+egg_allergy+other_allergy+
                                             VOMITING+
                                             SPITTING+STOOLFREQ+STOOLCONSIST+
                                             STOOLCOLOUR+
                                           GASWIND+SCORADMAN+num_ab+num_inf),
                               data = meta_12M, na.action = na.omit)
partial_rda_treatment_12M$anova$`Pr(>F)` <- p.adjust(
  partial_rda_treatment_12M$anova$`Pr(>F)`, method = "fdr") 
# adjust p-values for multiple testing
print(anova(partial_rda_treatment_12M,by="term"))

perc <- round(100*(summary(partial_rda_treatment_12M)$cont$importance[2, 1:2]), 2)

#Figure S5D
loadings<-rbind(scores(partial_rda_treatment_12M, display = "bp", scaling = 0),
                scores(partial_rda_treatment_12M, display = "species", scaling = 0))
loading_contributions<-rowSums(abs(loadings))
rel_loading_contributions<-loading_contributions/sum(loading_contributions)
rel_loading_contributions[1]

ordered_rel_loading_contr<-rel_loading_contributions[order(rel_loading_contributions,
                                                           decreasing=TRUE)]
cum_loading_contrib<-vector(mode="numeric",
                            length=length(ordered_rel_loading_contr))
for (i in 1:length(ordered_rel_loading_contr)){
  cum_loading_contrib<-cumsum(ordered_rel_loading_contr[1:i])
}
ind_80<-min(which(cum_loading_contrib>0.80))

sc_bp<-scores(partial_rda_treatment_12M, display = "bp", scaling = 0)
sc_sp<-scores(partial_rda_treatment_12M, display = "species", scaling = 0)
sc_si<-scores(partial_rda_treatment_12M, display = "sites", scaling = 0)

plot(partial_rda_treatment_12M,
     scaling = 2, # set scaling type 
     type = "none", # this excludes the plotting of any points from the results
     frame = FALSE,
     # set axis limits
     xlim = c(-0.3,0.3), 
     ylim = c(-0.7,0.7),
     # label the plot (title, and axes)
     main = "proteome - 12 months",
     xlab = paste0("RDA1 (", perc[1], "%)"), 
     ylab = paste0("PC1 (", perc[2], "%)") 
)
# add points for species scores
points(sc_sp, 
       pch = 22, # set shape (here, square with a fill colour)
       col = "orange",
       cex = 1.2)

# add points for site scores
points(sc_si, 
       pch = 16, # set shape (here, circle with a fill colour)
       col = as.factor(meta_12M$treatment),
       cex = 1.2) # size
# add arrows for effects of the expanatory variables
arrows(0,0, # start them from (0,0)
       sc_bp[,1], sc_bp[,2], # end them at the score value
       col = "blue", 
       lwd = 1)
# add text labels for arrows
text(x = sc_bp[,1] -0.1, # adjust text coordinate to avoid overlap with arrow tip
     y = sc_bp[,2] - 0.03, 
     labels = rownames(sc_bp), 
     col = "blue", 
     cex = 1, 
     font = 1)
legend(-1.6,0.4,legend=c("A", "B"),
       col=c("black","red"),
       pch = 16,
       cex=0.8)