################################################################################
#Make a heatmap for the LEfSe output at visit 6 months.                        #
################################################################################
#Author: Diana Hendrickx#
#########################
#input:
#table_6M.txt: output of prepare_for_LEfSe for visit 6 months

setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

library(limma)
library(ComplexHeatmap)
library(readr)

table_6M <- read_delim("table_6M.txt", "\t", escape_double = FALSE, 
                       col_names = FALSE, trim_ws = TRUE)

data<-as.matrix(table_6M[3:65,-1])
class(data)<-"numeric"
treatment<-table_6M[1,-1]
subjectnr<-table_6M[2,-1]
taxonomy<-table_6M[3:65,1]
taxonomy_table<-strsplit2(taxonomy$X1,split="[|]")

ind_Clostridia<-which(taxonomy_table[,3]=="Clostridia")
ind_Burkholderiales<-which(taxonomy_table[,4]=="Burkholderiales")
ind_Sutterellaceae<-which(taxonomy_table[,5]=="Sutterellaceae")
ind_Monoglobaceae<-which(taxonomy_table[,5]=="Monoglobaceae")
ind_Monoglobales<-which(taxonomy_table[,4]=="Monoglobales")
ind_Butyricicoccaceae<-which(taxonomy_table[,5]=="Butyricicoccaceae")
ind_Lactobacillales_unclassified<-which(taxonomy_table[,5]=="Lactobacillales_unclassified")
ind_Morganellaceae<-which(taxonomy_table[,5]=="Morganellaceae")
ind_Enterococcaceae<-which(taxonomy_table[,5]=="Enterococcaceae")
ind_Lactobacillaceae<-which(taxonomy_table[,5]=="Lactobacillaceae")
ind_Actinobacteriota<-which(taxonomy_table[,2]=="Actinobacteriota")
ind_Bifidobacteriaceae<-which(taxonomy_table[,5]=="Bifidobacteriaceae")
ind_Bifidobacteriales<-which(taxonomy_table[,4]=="Bifidobacteriales")
ind_Actinobacteria<-which(taxonomy_table[,3]=="Actinobacteria")

Clostridia<-colSums(data[ind_Clostridia,])
Burkholderiales<-colSums(data[ind_Burkholderiales,])
Sutterellaceae<-data[ind_Sutterellaceae,] #only 1 row
Monoglobaceae<-data[ind_Monoglobaceae,] #only 1 row
Monoglobales<-data[ind_Monoglobales,] #only 1 row
Butyricicoccaceae<-data[ind_Butyricicoccaceae,] #only 1 row
Lactobacillales_unclassified<-data[ind_Lactobacillales_unclassified,] #only 1 row
Morganellaceae<-data[ind_Morganellaceae,] #only 1 row
Enterococcaceae<-data[ind_Enterococcaceae,] #only 1 row
Lactobacillaceae<-data[ind_Lactobacillaceae,] #only 1 row
Actinobacteriota<-colSums(data[ind_Actinobacteriota,])
Bifidobacteriaceae<-data[ind_Bifidobacteriaceae,] #only 1 row
Bifidobacteriales<-data[ind_Bifidobacteriales,] #only 1 row
Actinobacteria<-colSums(data[ind_Actinobacteria,])

data_heatmap<-rbind(Clostridia,
                    Burkholderiales,
                    Sutterellaceae,
                    Monoglobaceae,
                    Monoglobales,
                    Butyricicoccaceae,
                    Lactobacillales_unclassified,
                    Morganellaceae,
                    Enterococcaceae,
                    Lactobacillaceae,
                    Actinobacteriota,
                    Bifidobacteriaceae,
                    Bifidobacteriales,
                    Actinobacteria)

index1<-which(treatment=="A")
index2<-which(treatment=="B")
index<-c(index1,index2)
data_heatmap<-data_heatmap[,index]
subjectnr_heatmap<-subjectnr[index]
colnames(data_heatmap)<-subjectnr_heatmap
mean<-apply(data_heatmap,1,mean)
sd<-apply(data_heatmap,1,sd)
data_heatmap_zscore<-(data_heatmap-mean)/sd

group_heatmap<-c(rep("AAF-syn",length(index1)),rep("AAF",length(index2)))
df<-data.frame(type=group_heatmap)
ha<-HeatmapAnnotation(df=df,col=list(type=c("AAF-syn"="darkgreen","AAF"="orange")),
                      annotation_legend_param = list(title_gp=gpar(fontsize=14),
                                                     labels_gp=gpar(fontsize=14)))

h=Heatmap(data_heatmap_zscore,
           show_column_names = FALSE,
           show_row_names = TRUE,
           cluster_rows = FALSE,
           cluster_columns = FALSE,
           column_title = "AAF-syn vs AAF",
           name = "Row Z-score",
           top_annotation = ha,
           heatmap_legend_param = list(title_gp=gpar(fontsize=14),
                                       labels_gp=gpar(fontsize=14)))