partial_RDA.R:
- Partial RDA with intervention as explanatory variable,adjusting for other environmental factors.
- Figure S5E: partial RDA at visit 6 months
- Figure S5F: partial RDA at visit 12 months

prepare_for_LEfSe.R:
- Prepare input files for the LEfSe Galaxy module 

LMM.R:
- Linear mixed model (LMM) analysis on core taxa, with intervention, visit and intervention x visit as fixed effects and subject as random effect.

heatmap_6M.R: 
- Make a heatmap for the LEfSe output at visit 6 months (Figure S1A).

heatmap_12M.R: 
- Make a heatmap for the LEfSe output at visit 12 months (Figure S1B).